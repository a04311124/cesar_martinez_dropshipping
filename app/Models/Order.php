<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_id',
        'date_order',
        'total',
        'product_id',
        'status'
    ];

    public function Client() {
        return $this->belongsTo(Client::class);
    }

    public function Product() {
        return $this->belongsTo(Product::class);
    }

}
