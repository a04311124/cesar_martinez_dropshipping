<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrecuentQuestions extends Model
{
    use HasFactory;

    protected $fillable = [
        'ask',
        'answer',
        'status'
    ];
}
