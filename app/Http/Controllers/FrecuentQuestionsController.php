<?php

namespace App\Http\Controllers;

use App\Models\FrecuentQuestions;
use Illuminate\Http\Request;

class FrecuentQuestionsController extends Controller
{
    public function index() {

        $frecuentquestions = FrecuentQuestions::orderBy('id', 'ASC')->paginate(10);

        return view('admin.frecuentquestion.index', compact('frecuentquestions'));
    }

    function create(Request $request)
    {
        //validar info
        $validateData = $request->validate([
            'ask' => 'required',
            'answer' => 'required'
        ],[
            'ask.required' => 'Ask Required.',
            'answer.required' => 'Answer Image Required.'
        ]);
     
        //crear registro
        $frecuentquestions = FrecuentQuestions::create([
            'ask' => $validateData['ask'],
            'answer' => $validateData['answer'],
            'status' => 1
        ]);
       
        if($frecuentquestions->save()){
            //Agregar registro bitacora

            return redirect(route('frecuentquestions'));

        }
        else{


            return redirect(route('frecuentquestions'));
        }

    }

    function update(Request $request)
    {
        $frecuentquestions = FrecuentQuestions::findOrFail($request->id);

         //validar info
         $validateData = $request->validate([
            'ask' => 'required',
            'answer' => 'required'
        ],[
            'ask.required' => 'Ask Required.',
            'answer.required' => 'Answer Required.'
        ]);

        $frecuentquestions->ask = $validateData['ask'];
        $frecuentquestions->answer = $validateData['answer'];

        $frecuentquestions->save();

        return redirect(route('frecuentquestions'));
    }

    function terminate(Request $request){

        $frecuentquestions = FrecuentQuestions::findOrFail($request->id);

        //validar la información
        $validateData = $request->validate([

            'status' => 'required|numeric'
        ]);

        //actualiza el registro        
        $frecuentquestions->status = $validateData['status'];

        $frecuentquestions->save();

        //TODO Agregar registro a la bitacora

        //retornar la vista
        return redirect(route('frecuentquestions'));
    }
}
