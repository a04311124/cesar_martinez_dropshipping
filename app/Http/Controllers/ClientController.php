<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    function index(){

        $clients = Client::paginate(10);

        return view('admin.clients.index', compact('clients'));
    }
    function create(Request $request)
    {
        //validar info
        $validateData = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ],[
            'name.required' => 'Client Name Required.',
            'address.required' => 'Client Address Required.',
            'email.required' => 'Client Email Required.',
            'phone.required' => 'Client Phone Required.'
            


        ]);
     
        //crear registro
        $client = Client::create([
            'name' => $validateData['name'],
            'address' => $validateData['address'],
            'email' => $validateData['email'],
            'phone' => $validateData['phone'],
            'status' => 1
        ]);
       
        if($client->save()){
            //Agregar registro bitacora

            return redirect(route('clients'));

        }
        else{


            return redirect(route('clients'));
        }

    }

    function update(Request $request)
    {
        $client = Client::findOrFail($request->id);

         //validar info
         $validateData = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'phone' => 'required',

        ],[
            'name.required' => 'Client Name Required.',
            'address.required' => 'Client Address Required.',
            'email.required' => 'Client Email Required.',
            'phone.required' => 'Client Phone Required.'
        ]);

        $client->name = $validateData['name'];
        $client->address = $validateData['address'];
        $client->email = $validateData['email'];
        $client->phone = $validateData['phone'];

        $client->save();

        return redirect(route('clients'));
    }

    function terminate(Request $request){

        $client = Client::findOrFail($request->id);

        //validar la información
        $validateData = $request->validate([

            'status' => 'required|numeric'
        ]);

        //actualiza el registro        
        $client->status = $validateData['status'];

        $client->save();

        //TODO Agregar registro a la bitacora

        //retornar la vista
        return redirect(route('clients'));
    }

}
