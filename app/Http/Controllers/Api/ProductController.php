<?php

namespace App\Http\Controllers\Api;

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function list(){
        
        $products = Product::all();

        $list = [];

        foreach($products as $product)
        {
            $object = [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'price' => $product->price,
                'available' => $product->available,
                'status'=> $product->status,
                'image' => $product->image,
                'category' => $product->category
            ];
            array_push($list,$object);
        }

        return response()->json($list);

    }

    public function item($id){
        
        $product = Product::where('id', '=', $id)->first();
        
            $object = [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'price' => $product->price,
                'available' => $product->available,
                'status'=> $product->status,
                'image' => $product->image,
                'category' => $product->category
            ];;
        
        return response()->json($object);

    }

    public function create(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'available' => 'required|numeric',
            'image' => 'required',
            'category_id' => 'required'
        ]);
     
        //crear registro
        $product = Product::create([
            'name' => $validateData['name'],
            'description' => $validateData['description'],
            'price' => $validateData['price'],
            'available' => $validateData['available'],
            'image' => $validateData['image'],
            'category_id' => $validateData['category_id'],
            'status' => 1
        ]);
       
        $object = [
            'response' => 'ok',
            'message' => 'Se añadio el producto correctamente',
            'data' => $product
        ];

        return response()->json($object);
        
    }

    public function update(Request $request)
    {

    }


}