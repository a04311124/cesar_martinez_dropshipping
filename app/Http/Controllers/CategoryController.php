<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index() {

        $categories = Category::orderBy('id', 'ASC')->paginate(10);

        return view('admin.categories.index', compact('categories'));
    }

    function create(Request $request)
    {
        //validar info
        $validateData = $request->validate([
            'name' => 'required'
        ],[
            'name.required' => 'Category Name Required.'

        ]);
     
        //crear registro
        $category = Category::create([
            'name' => $validateData['name'],
            'status' => 1
        ]);
       
        if($category->save()){
            //Agregar registro bitacora

            return redirect(route('categories'));

        }
        else{


            return redirect(route('products'));
        }

    }

    function update(Request $request)
    {
        $category = Category::findOrFail($request->id);

         //validar info
         $validateData = $request->validate([
            'name' => 'required'
        ],[
            'name.required' => 'Category Name Required.'
        ]);

        $category->name = $validateData['name'];
        $category->save();

        return redirect(route('categories'));
    }

    function terminate(Request $request){

        $category = Category::findOrFail($request->id);

        //validar la información
        $validateData = $request->validate([

            'status' => 'required|numeric'
        ]);

        //actualiza el registro        
        $category->status = $validateData['status'];

        $category->save();

        //TODO Agregar registro a la bitacora

        //retornar la vista
        return redirect(route('categories'));
    }
}
