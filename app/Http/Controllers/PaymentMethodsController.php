<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethods;
use Illuminate\Http\Request;

class PaymentMethodsController extends Controller
{
    public function index() {

        $paymentmethods = PaymentMethods::orderBy('id', 'ASC')->paginate(10);

        return view('admin.paymentmethods.index', compact('paymentmethods'));
    }

    function create(Request $request)
    {
        //validar info
        $validateData = $request->validate([
            'name' => 'required',
            'imagen' => 'required'
        ],[
            'name.required' => 'Payment Methods Name Required.',
            'imagen.required' => 'Payment Methods Image Required.'
        ]);
     
        //crear registro
        $paymentmethods = PaymentMethods::create([
            'name' => $validateData['name'],
            'imagen' => $validateData['imagen'],
            'status' => 1
        ]);
       
        if($paymentmethods->save()){
            //Agregar registro bitacora

            return redirect(route('paymentmethods'));

        }
        else{


            return redirect(route('paymentmethods'));
        }

    }

    function update(Request $request)
    {
        $paymentmethods = PaymentMethods::findOrFail($request->id);

         //validar info
         $validateData = $request->validate([
            'name' => 'required',
            'imagen' => 'required'
        ],[
            'name.required' => 'Payment Method Name Required.',
            'imagen.required' => 'Payment Method Image Required.'
        ]);

        $paymentmethods->name = $validateData['name'];
        $paymentmethods->imagen = $validateData['imagen'];

        $paymentmethods->save();

        return redirect(route('paymentmethods'));
    }

    function terminate(Request $request){

        $paymentmethods = PaymentMethods::findOrFail($request->id);

        //validar la información
        $validateData = $request->validate([

            'status' => 'required|numeric'
        ]);

        //actualiza el registro        
        $paymentmethods->status = $validateData['status'];

        $paymentmethods->save();

        //TODO Agregar registro a la bitacora

        //retornar la vista
        return redirect(route('paymentmethods'));
    }
}
