<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $levels = Level::where('id', '>', 1)->orderBy('name', 'ASC')->get();
        $users = User::all();

        return view('admin.users.index', compact('users', 'levels'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required|max:10',
            'email' => 'required',
            'password' => 'required',
            'level_id' => 'required|numeric'
        ]);

        $user = new User;

        $user->name = $validatedData['name'];
        $user->email =$validatedData['email'];
        $user->phone = $validatedData['phone'];
        $user->level_id = $validatedData['level_id'];
        $user->password = $validatedData['password'];

        if($user->save()){        

            return redirect(route('users'))->with('success', 'User added successfully.');
        } else{

            return redirect(route('users'))->with('error', 'Something went wrong, please try again.');
        }
    }
}
