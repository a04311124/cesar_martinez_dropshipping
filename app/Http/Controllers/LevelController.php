<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function index() {

        $levels = Level::orderBy('id', 'ASC')->paginate(10);

        return view('admin.levels.index', compact('levels'));
    }

    function create(Request $request)
    {
        //validar info
        $validateData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'code' => 'required'
        ],[
            'name.required' => 'Level Name Required.',
            'name.description' => 'Level Description Required.',
            'name.code' => 'Level Code Required.'
        ]);
     
        //crear registro
        $level = Level::create([
            'name' => $validateData['name'],
            'description' => $validateData['description'],
            'code' => $validateData['code'],
            'status' => 1
        ]);
       
        if($level->save()){
            //Agregar registro bitacora

            return redirect(route('levels'));

        }
        else{


            return redirect(route('levels'));
        }

    }

    function update(Request $request)
    {
        $level = Level::findOrFail($request->id);

         //validar info
         $validateData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'code' => 'required'
        ],[
            'name.required' => 'Level Name Required.',
            'name.description' => 'Level Description Required.',
            'name.code' => 'Level Code Required.'
        ]);

        $level->name = $validateData['name'];
        $level->description = $validateData['description'];
        $level->code = $validateData['code'];
        $level->save();

        return redirect(route('levels'));
    }
}
