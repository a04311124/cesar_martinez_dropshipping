<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use DateTime;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    function index(){

        $products = Product::where('status', '=', 1)->orderBy('category_id')->get();
        $categories = Category::where('status', '=', 1)->orderBy('name', 'ASC')->get();

        //dd($products);

        return view('admin.products.index', compact('products', 'categories'));
    }

    function terminated() {

        $products = Product::where('status', '=', 0)->get();
        $categories = Category::where('status', '=', 1)->orderBy('name', 'ASC')->get();

        //dd($products);

        return view('admin.products.terminated', compact('products', 'categories'));
    }


    function create(Request $request)
    {
        //validar info
        $validateData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'available' => 'required|numeric',
            'image' => 'required',
            'category_id' => 'required'
        ],[
            'name.required' => 'Product Name Required.',
            'name.description' => 'Product Description Required.',
            'name.price' => 'Product Price Required.',
            'name.available' => 'Product Available Required.',
            'name.image' => 'Product Image Required.',
            'name.category_id' => 'Product Category Required.'

        ]);
     
        //crear registro
        $product = Product::create([
            'name' => $validateData['name'],
            'description' => $validateData['description'],
            'price' => $validateData['price'],
            'available' => $validateData['available'],
            'image' => $validateData['image'],
            'category_id' => $validateData['category_id'],
            'status' => 1
        ]);
       
        if($product->save()){
            //Agregar registro bitacora

            return redirect(route('products'));

        }
        else{


            return redirect(route('products'));
        }

    }

    function update(Request $request)
    {
        $product = Product::findOrFail($request->id);

         //validar info
         $validateData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'available' => 'required|numeric',
            'image' => 'required',
            'category_id' => 'required'
        ],[
            'name.required' => 'Product Name Required.',
            'name.description' => 'Product Description Required.',
            'name.price' => 'Product Price Required.',
            'name.available' => 'Product Available Required.',
            'name.image' => 'Product Iamge Required.',
            'name.category_id' => 'Product Category Required.'
        ]);

        $product->name = $validateData['name'];
        $product->description = $validateData['description'];
        $product->price = $validateData['price'];
        $product->available = $validateData['available'];
        $product->image = $validateData['image'];
        $product->category_id = $validateData['category_id'];
        $product->save();

        return redirect(route('products'));
    }

    function terminate(Request $request){

        $product = Product::findOrFail($request->id);

        //validar la información
        $validateData = $request->validate([

            'status' => 'required|numeric'
        ]);

        //actualiza el registro        
        $product->status = $validateData['status'];

        $product->save();

        //TODO Agregar registro a la bitacora

        //retornar la vista
        return redirect(route('products'));
    }

    function buy(Request $request)
    {
        $product = Product::findOrFail($request->id);

         //validar info
         

        $product->available = ($product->available) - 1;
        $product->save();

        $order = Order::create([
            'client_id' => random_int(1,20),
            'date_order' => new DateTime(),
            'total' => $request['price'],
            'status' => 1,
            'product_id' => $request['id']
        ]);

        $order->save();

        return redirect(route('products'));
    }
}
