<?php

namespace App\Http\Controllers;
use App\Models\Product;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       // $products = Product::where('status', '=', 0)->get();

        $products = Product::where('status', '=', 1)->where('available', '>', '0')->orderBy('category_id')->get();
        
        $productlast = Product::orderBy('id','desc')->take(1)->get();

        return view('index', compact('products'), compact('productlast'));
        
    }
}
