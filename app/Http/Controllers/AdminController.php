<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Client;
use App\Models\FrecuentQuestions;
use App\Models\Level;
use App\Models\Order;
use App\Models\PaymentMethods;
use App\Models\Product;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        //->except(['index']);
    }

    public function index() {

        $products = Product::all()->count();
        $clients = Client::all()->count();
        $levels = Level::all()->count();
        $orders = Order::all()->count();
        $users = User::all()->count();
        $categories = Category::all()->count();
        $paymentmethods = PaymentMethods::all()->count();
        $frecuentquestions = FrecuentQuestions::all()->count();

        return view('admin.index', compact('products', 'clients', 'levels', 'orders', 'users', 'categories','paymentmethods', 'frecuentquestions'));
    }

    public function sections() {

        $sections = Section::all();


        return view('admin.sections.index', compact('sections'));
    }
}
