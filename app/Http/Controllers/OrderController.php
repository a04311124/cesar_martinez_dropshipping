<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    function index(){

        $orders = Order::paginate(10);
        return view('admin.orders.index', compact('orders'));
    }
}
