<?php
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\FrecuentQuestionsController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PaymentMethodsController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\UserController;
use App\Models\FrecuentQuestions;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', [HomeController::class, 'index'])->name('index');

Auth::routes();

Route::prefix('/admin')->group(function(){
	Route::get('/', [AdminController::class, 'index'])->name('dash');

	Route::prefix('/products')->group(function() {
		Route::get('/', [ProductController::class, 'index'])->name('products');
		Route::post('/create', [ProductController::class, 'create'])->name('products.create');
		Route::post('/update', [ProductController::class, 'update'])->name('products.update');
		Route::get('/terminated', [ProductController::class, 'terminated'])->name('products.terminated');
		Route::post('/terminate', [ProductController::class, 'terminate'])->name('products.terminate');
		Route::post('/buy', [ProductController::class, 'buy'])->name('products.buy');

	});

    Route::prefix('/clients')->group(function() {
		Route::get('/', [ClientController::class, 'index'])->name('clients');
		Route::post('/create', [ClientController::class, 'create'])->name('clients.create');
		Route::post('/update', [ClientController::class, 'update'])->name('clients.update');
		Route::post('/terminate', [ClientController::class, 'terminate'])->name('clients.terminate');
	});

    Route::prefix('/orders')->group(function() {
		Route::get('/', [OrderController::class, 'index'])->name('order');
		Route::post('/create', [OrderController::class, 'create'])->name('orders.create');
	});

	Route::prefix('/engines')->group(function(){
		Route::get('/', [EngineController::class, 'index'])->name('engines');
	});

	Route::prefix('/levels')->group(function(){
		Route::get('/', [LevelController::class, 'index'])->name('levels');
		Route::post('/update', [LevelController::class, 'update'])->name('levels.update');
		Route::post('/create', [LevelController::class, 'create'])->name('levels.create');
	});

	Route::get('/sections', [AdminController::class, 'sections'])->name('sections');
	Route::post('/sections', [AdminController::class, 'update'])->name('sections.update');

	Route::prefix('/types')->group(function(){
		Route::get('/', [TypeController::class, 'index'])->name('types');
	});

	Route::prefix('/users')->group(function() {
		Route::get('/', [UserController::class, 'index'])->name('users');
		Route::post('/create', [UserController::class, 'store'])->name('users.create');
	});

	Route::prefix('/categories')->group(function() {
		Route::get('/', [CategoryController::class, 'index'])->name('categories');
		Route::post('/create', [CategoryController::class, 'create'])->name('categories.create');
		Route::post('/update', [CategoryController::class, 'update'])->name('categories.update');
		Route::post('/terminate', [CategoryController::class, 'terminate'])->name('categories.terminate');
	});

	Route::prefix('/paymentmethods')->group(function() {
		Route::get('/', [PaymentMethodsController::class, 'index'])->name('paymentmethods');
		Route::post('/create', [PaymentMethodsController::class, 'create'])->name('paymentmethods.create');
		Route::post('/update', [PaymentMethodsController::class, 'update'])->name('paymentmethods.update');
		Route::post('/terminate', [PaymentMethodsController::class, 'terminate'])->name('paymentmethods.terminate');
	});

	Route::prefix('/frecuentquestions')->group(function() {
		Route::get('/', [FrecuentQuestionsController::class, 'index'])->name('frecuentquestions');
		Route::post('/create', [FrecuentQuestionsController::class, 'create'])->name('frecuentquestions.create');
		Route::post('/update', [FrecuentQuestionsController::class, 'update'])->name('frecuentquestions.update');
		Route::post('/terminate', [FrecuentQuestionsController::class, 'terminate'])->name('frecuentquestions.terminate');
	});

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
