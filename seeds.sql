INSERT INTO levels (id, name, code, description, status, created_at, updated_at) VALUES
(1,'User1','UL1','Usuario Level 1',1,'2023-07-01 10:15:00', '2023-07-03 14:30:00'),
(2,'User2','UL2','Usuario Level 2',1,'2023-07-01 10:15:00', '2023-07-03 14:30:00'),
(3,'User3','UL3','Usuario Level 3',1,'2023-07-01 10:15:00', '2023-07-03 14:30:00'),
(4,'Administrator','AD','Usuario Administrator',1,'2023-07-01 10:15:00', '2023-07-03 14:30:00'),
(5,'RootAdmin','RAD','Usuario Root Admin',1,'2023-07-01 10:15:00', '2023-07-03 14:30:00');

INSERT INTO products (id, name, description, price, available, status, created_at, updated_at, image, category_id) VALUES
(1, 'Echo Dot (3ra generación)', 'Echo Dot (3ra generación) - Bocina inteligente con Alexa, negro', '999.00', 15, 1, '2023-08-01 14:20:47', '2023-08-10 14:45:22', 'https://images-na.ssl-images-amazon.com/images/I/61Rr8uxmREL._AC_UL160_SR160,160_.jpg', 3),
(2, 'Consola Nintendo Switch', 'Consola Nintendo Switch 1.1 + Mario Kart 8 Deluxe + 3 Meses Nintendo Online - Special Limited Edition', '9,199', 10, 1, NULL, '2023-08-10 10:59:46', 'https://m.media-amazon.com/images/I/71osBiI7iML._AC_SL1200_.jpg', 2),
(3, 'Pantalla Hisense 55\"', 'Pantalla Hisense 55\" 4k smart tv 55A6H google TV (2022)', '7,299.00', 0, 1, NULL, '2023-08-10 13:09:25', 'https://m.media-amazon.com/images/I/61mL5ArW9VL._AC_SL1200_.jpg', 1),
(4, 'Dragon Ball Xenoverse 2 - Standard Edition', '¡Dragon Ball Xenoverse 2 llega a Nintendo Switch con nuevas funcionalidades!', '499.00', 1, 1, NULL, '2023-08-10 12:47:32', 'https://m.media-amazon.com/images/I/71tXAV2WIPS._AC_SL1500_.jpg', 2),
(5, 'MyDear Cuerda para Saltar', 'MyDear Cuerda para Saltar, Fácilmente Ajustable Manijas Cómodas, 300CM Cable, Rodamientos de Bolas de Acero Ergonómico y Mango Antideslizante, Cuerda Ligera', '109.00', 5, 1, NULL, '2023-08-10 12:47:42', 'https://m.media-amazon.com/images/I/41VPkyVIDrL._AC_.jpg', 19),
(6, 'Fire TV Stick 4K con control remoto', 'Fire TV Stick 4K con control remoto por voz Alexa (incluye control de TV) y Dolby Vision', '1,499.00', 2, 0, '2023-08-01 16:08:17', '2023-08-10 14:45:51', 'https://m.media-amazon.com/images/I/41NjSs2gmvL._AC_SL1000_.jpg', 1),
(7, 'Apple iPhone 11', 'Apple iPhone 11, Totalmente Desbloqueado, 64GB, Negro (Reacondicionado)', '6,347.00', 1, 1, '2023-08-01 14:20:47', '2023-08-10 12:47:48', 'https://m.media-amazon.com/images/I/31PpUfTCiFL._AC_.jpg', 4),
(8, 'MSI Thin GF63 15.6\" 144Hz Gaming Laptop', 'MSI Thin GF63 15.6\" 144Hz Gaming Laptop: 12th Gen Intel Core i7, NVIDIA GeForce RTX 4050, 16GB DDR4, 512GB NVMe SSD, Type-C, Cooler Boost 5, Win11 Home: Black 12VE-066US', '17,990.87', 2, 1, '2023-08-01 14:20:47', '2023-08-10 14:45:37', 'https://m.media-amazon.com/images/I/71JqyTBiXrL._AC_SL1500_.jpg', 1),
(9, 'VEKENSEE Soporte para Laptop con Soporte para Celular', 'VEKENSEE Soporte para Laptop con Soporte para Celular, 2 en 1 Múltiples Ángulos Ajustable Soporte Portátil y Soporte para Teléfono Desmontable, Soporte Ventilado Portátil con Soporte Móvil (ABS Negro)', '169.00', 1, 1, '2023-08-01 14:20:47', '2023-08-10 12:48:11', 'https://m.media-amazon.com/images/I/71zPOsV6EiL._AC_SL1500_.jpg', 4),
(10, 'Apple iPhone 12', 'Apple iPhone 12, 128 GB, Negro - Totalmente Desbloqueado (Reacondicionado)', '8,969.00', 2, 0, '2023-08-08 19:32:38', '2023-08-10 12:49:39', 'https://m.media-amazon.com/images/I/41bIlvE1rdL._AC_SL1000_.jpg', 4),
(11, 'HUAWEI Band 8', 'HUAWEI Band 8, Larga Duración de 2 semanas, SMS/Whatsapp de Respuesta rápida，Negro', '849.00', 15, 1, '2023-08-08 19:35:46', '2023-08-10 12:48:26', 'https://m.media-amazon.com/images/I/51ftyQ4Q54L._AC_SL1000_.jpg', 4),
(12, 'CeraVe AM Loción hidratante facial SPF 30', 'CeraVe AM Loción hidratante facial SPF 30 | Hidratante facial sin aceite con protector solar | No comedogénico | 3 onzas', '125.00', 15, 1, '2023-08-09 15:31:31', '2023-08-10 12:48:33', 'https://m.media-amazon.com/images/I/71J0BtuYB2L._SL1500_.jpg', 19),
(13, 'Jurassic World Toys Camp Cretaceous Dinosaur Toy', 'Jurassic World Toys Camp Cretaceous Dinosaur Toy, Stomp  Escape Tyrannosaurus Rex Action Figure with Stomping Motion', '1,032.06', 15, 1, '2023-08-11 02:07:01', '2023-08-11 02:07:01', 'https://m.media-amazon.com/images/I/61alpMxQEkL._AC_SL1500_.jpg', 14),
(14, 'Birdman Falcon Performance Proteina Premium Alto Rendimiento En Polvo', 'Birdman Falcon Performance Proteina Premium Alto Rendimiento En Polvo, 30gr proteina y 3gr Creatina por porción, Sin Inflamacion, Sin Acne, Sabor Choco Bronze | 50 servicios | 1.9kg', '1,529.00', 2, 1, '2023-08-11 02:08:34', '2023-08-11 02:08:34', 'https://m.media-amazon.com/images/I/510JvPfO9+L._AC_SL1000_.jpg', 19),
(15, '(2023 Modelo Nuevo)Audífonos bluetooth CHEELOM', '(2023 Modelo Nuevo)Audífonos bluetooth CHEELOM, Audífonos Inalámbricos de entretenimiento,Impermeable y Reduce el Ruido CVC8.0, mini audífonos deportivos con interfaz con micrófono(Garantía de un año)', '199.00', 15, 1, '2023-08-11 02:11:27', '2023-08-11 02:11:27', 'https://m.media-amazon.com/images/I/61uJj7-1HoL._AC_SL1500_.jpg', 1),
(16, 'GIGABYTE Tarjeta gráfica GeForce RTX 4060 Ti Eagle 8G', 'GIGABYTE Tarjeta gráfica GeForce RTX 4060 Ti Eagle 8G, 3 Ventiladores WINDFORCE, 8 GB 128 bits GDDR6, GV-N406TEAGLE-8GD', '8,282.28', 5, 1, '2023-08-11 02:20:13', '2023-08-11 02:20:13', 'https://m.media-amazon.com/images/I/712setWp52L._AC_SL1500_.jpg', 1),
(17, 'he Legend of Zelda: Tears of the Kingdom - Nintendo Switch', 'he Legend of Zelda: Tears of the Kingdom - Nintendo Switch', '1,140.00', 15, 1, '2023-08-11 02:21:26', '2023-08-11 02:21:26', 'https://m.media-amazon.com/images/I/71glcphYY0L._AC_SL1500_.jpg', 2),
(18, 'Ninja - Freidora de aire, Negro/Gris, 3,78L, 1, 4.9', 'Ninja - Freidora de aire, Negro/Gris, 3,78L, 1, 4.9', '1,679.00', 5, 1, '2023-08-11 02:32:03', '2023-08-11 02:32:03', 'https://m.media-amazon.com/images/I/71+8uTMDRFL._AC_SL1500_.jpg', 18),
(19, 'Ninja - Professional BL610 - Licuadora de 1000 vatios', 'Ninja - Professional BL610 - Licuadora de 1000 vatios - Tecnología de trituración total', '1,689.14', 8, 1, '2023-08-11 02:33:42', '2023-08-11 02:33:42', 'https://m.media-amazon.com/images/I/71iD5RyhuaL._AC_SL1500_.jpg', 18),
(20, 'X Rocker, Silla Gamer 5125401 2.1, Negro/Rojo', 'X Rocker, Silla Gamer 5125401 2.1, Negro/Rojo', '5,485.00', 1, 1, '2023-08-11 04:00:41', '2023-08-11 04:00:41', 'https://m.media-amazon.com/images/I/61pB1hNDxeL._AC_SL1500_.jpg', 2);

INSERT INTO categories (id, name, status, created_at, updated_at) VALUES
(1, 'Electronics', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(2, 'VideoGames', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(3, 'Device Accesories', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(4, 'Cell Phones & Accesories', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(5, 'Consumer Electronics', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(6, 'Grocery', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(7, 'Health & Personal Care', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(8, 'Home & Garden', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(9, 'Music and DVD', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(10, 'Musical Instruments', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(11, 'Office Products', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(12, 'Outdoors', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(13, 'Music and DVD', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(14, 'Toys & Games', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(15, 'Watches', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(16, 'Tools & Games', 1, '2023-08-08 01:27:32', '2023-08-08 01:27:32'),
(17, 'Clothing, Shoes, and Jewelry', 1, '2023-08-08 02:56:12', '2023-08-08 02:56:12'),
(18, 'Home and Kitchen', 1, '2023-08-08 02:56:12', '2023-08-08 02:56:12'),
(19, 'Beauty and Personal Care', 1, '2023-08-08 02:56:12', '2023-08-08 02:56:12'),
(20, 'Books', 1, '2023-08-08 02:56:12', '2023-08-08 02:56:12');


INSERT INTO clients (id, name, address, email, phone, status, created_at, updated_at) VALUES
(1, 'Juan Pérez', 'Calle Principal 123', 'juan@example.com', '555-123-4567', 1, '2023-08-08 03:04:33', '2023-08-08 03:04:33'),
(2, 'María González', 'Avenida Central 456', 'maria@example.com', '555-987-6543', 1, '2023-07-01 17:45:00', '2023-07-05 16:15:00'),
(3, 'Luis Sánchez', 'Calle Secundaria 789', 'luis@example.com', '555-555-5555', 1, '2023-07-02 20:10:00', '2023-07-05 01:20:00'),
(4, 'Ana Romero', 'Avenida Principal 987', 'ana@example.com', '555-321-6549', 1, '2023-07-02 22:30:00', '2023-07-05 17:30:00'),
(5, 'Pedro Jiménez', 'Calle Secundaria 246', 'pedro@example.com', '555-777-8888', 1, '2023-07-03 18:20:00', '2023-07-04 22:45:00'),
(6, 'Laura Torres', 'Avenida Central 789', 'laura@example.com', '555-555-1234', 1, '2023-07-03 20:40:00', '2023-07-05 18:50:00'),
(7, 'Carlos Gómez', 'Calle Principal 456', 'carlos@example.com', '555-321-7890', 1, '2023-07-04 16:15:00', '2023-07-05 20:25:00'),
(8, 'Mariana Silva', 'Avenida Secundaria 987', 'mariana@example.com', '555-777-4567', 1, '2023-07-04 17:40:00', '2023-07-05 21:35:00'),
(9, 'Sergio Ramírez', 'Calle Central 123', 'sergio@example.com', '555-987-3210', 1, '2023-07-04 19:05:00', '2023-07-05 22:45:00'),
(10, 'Daniela Vargas', 'Avenida Principal 789', 'daniela@example.com', '555-123-7890', 1, '2023-07-04 20:30:00', '2023-07-05 23:55:00'),
(11, 'José Morales', 'Calle Secundaria 456', 'jose@example.com', '555-555-7890', 1, '2023-07-05 16:10:00', '2023-07-06 00:05:00'),
(12, 'Carolina Medina', 'Avenida Principal 123', 'carolina@example.com', '555-321-7890', 1, '2023-07-05 17:35:00', '2023-07-06 01:15:00'),
(13, 'Roberto Morales', 'Calle Principal 456', 'roberto@example.com', '555-123-4567', 1, '2023-07-06 03:55:00', '2023-07-05 09:35:00'),
(14, 'Gabriela Mendoza', 'Avenida Secundaria 789', 'gabriela@example.com', '555-777-1234', 1, '2023-07-05 18:00:00', '2023-07-06 02:25:00'),
(15, 'Ricardo Guzmán', 'Calle Principal 789', 'ricardo@example.com', '555-123-9876', 1, '2023-07-05 19:25:00', '2023-07-06 03:35:00'),
(16, 'Isabel Navarro', 'Avenida Central 246', 'isabel@example.com', '555-555-7890', 1, '2023-07-05 20:50:00', '2023-07-06 04:45:00'),
(17, 'Martín Castro', 'Calle Secundaria 123', 'martin@example.com', '555-321-9876', 1, '2023-07-05 22:15:00', '2023-07-06 05:55:00'),
(18, 'Valeria Paredes', 'Avenida Principal 456', 'valeria@example.com', '555-777-3210', 1, '2023-07-05 23:40:00', '2023-07-06 06:05:00'),
(19, 'Andrés López', 'Calle Central 789', 'andres@example.com', '555-123-6543', 1, '2023-07-06 01:05:00', '2023-07-05 07:15:00'),
(20, 'Julia Torres', 'Avenida Secundaria 123', 'julia@example.com', '555-321-6543', 1, '2023-07-06 02:30:00', '2023-07-05 08:25:00');


INSERT INTO `orders` (`id`, `client_id`, `date_order`, `total`, `status`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-07-01', '59.99', 1, 1, '2023-07-01 16:30:00', '2023-07-01 16:30:00'),
(2, 2, '2023-07-02', '159.98', 1, 2, '2023-07-01 17:45:00', '2023-07-02 20:15:00'),
(3, 3, '2023-07-03', '99.99', 1, 3, '2023-07-02 20:10:00', '2023-07-04 00:25:00'),
(4, 4, '2023-07-04', '199.95', 1, 4, '2023-07-02 22:30:00', '2023-07-04 18:45:00'),
(5, 5, '2023-07-05', '79.98', 1, 5, '2023-07-03 18:20:00', '2023-07-05 21:35:00'),
(6, 6, '2023-07-06', '49.99', 1, 6, '2023-07-03 20:40:00', '2023-07-05 23:50:00'),
(7, 7, '2023-07-07', '69.99', 1, 7, '2023-07-04 16:15:00', '2023-07-06 02:05:00'),
(8, 8, '2023-07-08', '119.99', 1, 8, '2023-07-04 17:40:00', '2023-07-06 03:15:00'),
(9, 9, '2023-07-09', '89.99', 1, 9, '2023-07-04 19:05:00', '2023-07-06 04:25:00'),
(10, 10, '2023-07-10', '39.99', 1, 10, '2023-07-04 20:30:00', '2023-07-06 05:35:00'),
(11, 11, '2023-07-11', '149.97', 1, 11, '2023-07-05 16:10:00', '2023-07-06 06:45:00'),
(12, 12, '2023-07-12', '79.99', 1, 12, '2023-07-05 17:35:00', '2023-07-06 08:55:00'),
(13, 13, '2023-07-13', '109.99', 1, 13, '2023-07-05 18:00:00', '2023-07-06 10:05:00'),
(14, 14, '2023-07-14', '129.99', 1, 14, '2023-07-05 19:25:00', '2023-07-06 11:15:00'),
(15, 15, '2023-07-15', '199.98', 1, 15, '2023-07-05 20:50:00', '2023-07-06 12:25:00'),
(16, 16, '2023-07-21', '79.99', 1, 16, '2023-07-06 03:55:00', '2023-07-06 18:15:00'),
(17, 17, '2023-07-17', '89.99', 1, 17, '2023-07-05 22:15:00', '2023-07-06 13:35:00'),
(18, 18, '2023-07-18', '59.99', 1, 18, '2023-07-05 23:40:00', '2023-07-06 14:45:00'),
(19, 19, '2023-07-19', '119.99', 1, 19, '2023-07-06 01:05:00', '2023-07-06 15:55:00'),
(20, 20, '2023-07-20', '99.99', 1, 20, '2023-07-06 02:30:00', '2023-07-06 17:05:00');

INSERT INTO payment_methods (id, name, imagen, status, created_at, updated_at) VALUES
(1, 'Credit and Debits', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQDlS9CR75KPnuxH5zk05I3sRH4CHCiZ-m1wg&usqp=CAU', 1, '2023-08-12 01:17:04', '2023-08-12 01:22:31'),
(2, 'Cash', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2ia_ppmKSmBV7rtsd93vtk_W7u1vYvg4PzA&usqp=CAU', 1, '2023-08-12 01:24:26', '2023-08-12 01:24:26'),
(3, 'Gift Cards', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRS02I1WMSWJukIOUVhoinHuJ6d9SnuqWXmeg&usqp=CAU', 1, '2023-08-12 01:24:50', '2023-08-12 01:24:50');

INSERT INTO users (id, name, email, phone, status, level_id, email_verified_at, password, remember_token, created_at, updated_at) VALUES
(1, 'Admin', 'admin@admin.com', '23123', 1, 4, NULL, '$2y$10$qUBxsG4kIc7fVGc9UVpNAeCvGxNWGKdcno9jK2RnOxsHSUqFXvAiO', NULL, '2023-08-12 01:37:33', '2023-08-12 01:37:33');


INSERT INTO frecuent_questions (id, ask, answer, status, created_at, updated_at) VALUES
(1, '¿Aceptan devoluciones?', 'Sí, aceptamos devoluciones dentro de los 30 días posteriores a la compra. Por favor, consulte nuestra política de devoluciones para obtener más detalles sobre cómo proceder.', 1, '2023-08-12 02:19:46', '2023-08-12 02:21:31'),
(2, '¿Cuánto tiempo tarda en llegar mi pedido?', 'El tiempo de envío depende de su ubicación y el método de envío seleccionado. Por lo general, los pedidos se entregan en un plazo de 3 a 7 días hábiles. Puede hacer un seguimiento del estado de su envío utilizando el número de seguimiento proporcionado.', 1, '2023-08-12 02:26:23', '2023-08-12 02:26:23'),
(3, '¿Qué métodos de pago aceptan?', 'Aceptamos tarjetas de crédito (Visa, MasterCard, American Express), PayPal y transferencias bancarias como métodos de pago seguros y confiables.', 1, '2023-08-12 02:26:45', '2023-08-12 02:26:45'),
(4, '¿Ofrecen envío internacional?', 'Sí, ofrecemos envío internacional a la mayoría de los países. Los costos y tiempos de envío pueden variar según la ubicación.', 1, '2023-08-12 02:27:02', '2023-08-12 02:27:02'),
(5, '¿Cómo puedo realizar un seguimiento de mi pedido?', 'Una vez que su pedido haya sido enviado, recibirá un correo electrónico con un número de seguimiento. Utilice este número en nuestro sitio web para rastrear el estado de su envío.', 1, '2023-08-12 02:27:17', '2023-08-12 02:27:17'),
(6, '¿Puedo cambiar mi dirección de envío después de realizar un pedido?', 'Si necesita cambiar la dirección de envío, comuníquese con nuestro equipo de atención al cliente lo antes posible. Haremos todo lo posible para hacer el cambio, siempre y cuando el pedido no haya sido enviado todavía.', 1, '2023-08-12 02:27:31', '2023-08-12 02:27:31'),
(7, '¿Qué hago si mi producto llega dañado?', 'Lamentamos escuchar eso. Por favor, póngase en contacto con nuestro equipo de atención al cliente y proporcione fotos del producto dañado. Resolveremos el problema y le enviaremos un reemplazo si es necesario.', 1, '2023-08-12 02:27:49', '2023-08-12 02:27:49'),
(8, '¿Cómo puedo cancelar mi pedido?', 'Si desea cancelar su pedido, contáctenos lo antes posible. Si el pedido aún no ha sido procesado para el envío, podemos cancelarlo y procesar un reembolso completo.', 1, '2023-08-12 02:28:04', '2023-08-12 02:28:04'),
(9, '¿Ofrecen garantía en sus productos?', 'Sí, muchos de nuestros productos vienen con garantía. Consulte la descripción del producto para conocer los detalles de la garantía. Si experimenta algún problema, estaremos encantados de ayudarle a resolverlo.', 1, '2023-08-12 02:28:19', '2023-08-12 02:28:19'),
(10, '¿Cómo puedo realizar una compra en su sitio web?', 'Navegue por nuestro catálogo y seleccione el producto que desea comprar. Haga clic en \"Agregar al carrito\". Revise su pedido y haga clic en \"Finalizar compra\".', 1, '2023-08-12 02:29:19', '2023-08-12 02:29:19'),
(11, '¿Tienen servicio al cliente por teléfono?', 'Sí, tenemos un equipo de atención al cliente disponible para ayudarlo. Puede comunicarse con nosotros en nuestro número de servicio al cliente durante el horario comercial.', 1, '2023-08-12 02:29:40', '2023-08-12 02:29:40'),
(12, '¿Puedo realizar un pedido por teléfono?', 'Preferimos que los pedidos se realicen a través de nuestro sitio web para garantizar la precisión y seguridad de la información. Sin embargo, si tiene dificultades, nuestro equipo de atención al cliente puede ayudarlo a realizar un pedido.', 1, '2023-08-12 02:29:54', '2023-08-12 02:29:54'),
(13, '¿Cómo puedo suscribirme al boletín informativo?', 'Puede suscribirse a nuestro boletín informativo en la parte inferior de nuestro sitio web. Simplemente ingrese su dirección de correo electrónico y recibirá actualizaciones sobre ofertas especiales, nuevos productos y más.', 1, '2023-08-12 02:30:09', '2023-08-12 02:30:09'),
(14, '¿Qué debo hacer si no recibo mi confirmación de pedido por correo electrónico?', 'Si no recibe la confirmación de su pedido en su bandeja de entrada, verifique su carpeta de correo no deseado o spam. Si aún no lo encuentra, comuníquese con nosotros y le proporcionaremos la información necesaria.', 1, '2023-08-12 02:30:24', '2023-08-12 02:30:24'),
(15, '¿Tienen una tienda física donde puedo ver los productos?', 'Actualmente operamos exclusivamente en línea y no tenemos tiendas físicas. Sin embargo, ofrecemos imágenes detalladas y descripciones precisas de nuestros productos en línea.', 1, '2023-08-12 02:30:39', '2023-08-12 02:30:39'),
(16, '¿Cómo puedo aplicar un cupón de descuento?', 'Puede aplicar un cupón de descuento en la página de pago antes de finalizar su compra. Ingrese el código del cupón en el campo designado y el descuento se aplicará automáticamente.', 1, '2023-08-12 02:30:55', '2023-08-12 02:30:55'),
(17, '¿Puedo cambiar mi pedido después de realizarlo?', 'Una vez que su pedido se haya completado, no se pueden realizar cambios en línea. Sin embargo, comuníquese con nuestro equipo de atención al cliente y haremos nuestro mejor esfuerzo para ayudarlo.', 1, '2023-08-12 02:31:11', '2023-08-12 02:31:11'),
(18, '¿Cómo puedo acceder a mi historial de pedidos?', 'Puede acceder a su historial de pedidos iniciando sesión en su cuenta en nuestro sitio web. Allí encontrará una lista de sus pedidos anteriores con detalles completos.', 1, '2023-08-12 02:31:52', '2023-08-12 02:31:52'),
(19, '¿Puedo comprar productos al por mayor?', 'Sí, ofrecemos opciones de compra al por mayor para ciertos productos. Comuníquese con nuestro equipo de ventas al por mayor para obtener más información y precios.', 1, '2023-08-12 02:32:06', '2023-08-12 02:32:06'),
(20, '¿Tienen programas de recompensas o membresía?', 'Actualmente no ofrecemos programas de recompensas o membresía. Sin embargo, asegúrese de suscribirse a nuestro boletín informativo para estar al tanto de cualquier actualización futura sobre ofertas y promociones especiales.', 1, '2023-08-12 02:32:21', '2023-08-12 02:32:40');
