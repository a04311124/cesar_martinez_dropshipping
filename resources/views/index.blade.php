@extends('layouts.site')

@section('title', 'Home')

@section('content')

@section('name', 'Products')

<section id="carousel">
	<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
		<div class="carousel-indicators">
			<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
			<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
			<button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
		</div>
		<div class="carousel-inner" style="background-color: black;">
			<div class="carousel-item active">
				<div class="container video-contain">
					<video autoplay muted loop>
						<source src="{{ asset('images/index/slider.mp4') }}" type="video/mp4" />
					</video>
					<div class="carousel-caption d-none d-md-block">
						<h5 style="color: #000000;">First slide label</h5>
						<p style="color: #000000;">Some representative placeholder content for the first slide.</p>
					</div>
				</div>
				
			</div>
			<div class="carousel-item">
				<div class="video-container">
					<img src="{{ asset('images/index/slider1.jpg') }}" class="d-block w-100" alt="...">
					<div class="carousel-caption d-none d-md-block">
						<h5 style="color: #000000;">Second slide label</h5>
						<p style="color: #000000;">Some representative placeholder content for the second slide.</p>
					</div>
				</div>
			</div>
			<div class="carousel-item">
				<div class="video-container">
					<img src="{{ asset('images/index/slider2.jpg') }}" class="d-block w-100" alt="...">
					<div class="carousel-caption d-none d-md-block">
						<h5>Third slide label</h5>
						<p>Some representative placeholder content for the third slide.</p>
					</div>
				</div>
			</div>
		</div>
		<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="visually-hidden">Next</span>
		</button>
	</div>	
</section>
<section id="columns">
	<div class="container">
		<div class="row my-5">
			<div class="col-md-4 col-sm-12">
			<div class="a-cardui deal-of-the-day" data-a-card-type="basic" id="CardInstancehAmk69UzOCCT4ZS6XIYYpw" data-card-metrics-id="deal-of-the-day_desktop-gateway-atf_8" data-mix-claimed="true"><div class="a-cardui-header">
				<h2 class="title mb-3">Trending Deals</h4>
					<div class="a-cardui-body">
						<div data-csa-c-type="item" data-csa-c-item-id="amzn1.deal.fdbe4ebd" data-csa-c-posx="1" data-csa-c-owner="DealsX" data-csa-c-id="k5oocl-bs3pp0-l1iqi5-gg16t1"><a class="a-spacing-small a-link-normal a-text-normal" href="#"><div class="a-section a-spacing-small">
							<div class="a-section a-spacing-mini center-image aok-block">
								<div class="image-container" style="height:230px">
									<img style="width:300px" src="https://m.media-amazon.com/images/I/31ZIKCEB2mL._AC_SY230_.jpg" data-a-hires="https://m.media-amazon.com/images/I/31ZIKCEB2mL._AC_SY460_.jpg">
							</div>
						</div>
							<div class="a-section a-spacing-micro _deal-of-the-day_style_badgeContainer__NehI_">
								<div class="a-row _deal-of-the-day_style_badgeContainer__2V4ps _deal-of-the-day_style_badgeOneLineContainer__SDZkK">
									<span style="background-color:#CC0C39;color:#ffffff" class="_deal-of-the-day_style_badgeLabel__USQoX"><span class="a-size-mini">Up to a -</span><span class="a-size-mini">19</span><span class="a-size-mini">%</span></span>
									<span style="background-color:#ffffff;color:#CC0C39" class="_deal-of-the-day_style_badgeMessage__3Xbyh">
									<span class="a-size-mini">
										Deal of the day</span></span>
								</div></div>
											
											<span class="a-truncate-full a-offscreen">Discount on Eyes</span>
										</span>
									</span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="a-cardui-footer">
					<a class="a-link-normal see-more truncate-1line" href="#">See more
					</a>
				</div>
			</div>
			
		</div>
		<div class="col-md-4 col-sm-12">
			<div>
				<div class="a-cardui-header">
				<h2 class="title mb-3">Keep Buying</h4>
					<div class="a-cardui-body">
						<div>
							<div class="a-section a-spacing-mini center-image aok-block">
								<div class="image-container" style="height:230px">
									<img style="width:200px;height:200px" src="https://m.media-amazon.com/images/I/61uZjwKiwOL.__AC_SX300_SY300_QL70_ML2_.jpg">
								</div>
							</div>
							
										</span>
									</span>
						</div>
					</div>
							
				</div>
					
				</div>
				<h4> 
					<span class="a-size-base-plus _cDEzb_titleTextMaxRows4_2Bk2T"> <strong>BIRDMAN</strong>  Birdman Falcon Performance Premium Protein Powder, 30gr Protein, 3gr Creatine, 45 Servings, Golden Vanilla Flavor</span>  
				</h4>
				<h3>
				<span aria-hidden="true"><span class="a-price-whole"><strong>$1,495.00</strong></span></span>
				</h3>
				<div class="a-cardui-footer">
					<a class="a-link-normal see-more truncate-1line" href="#">See more
					</a>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-12">
				<div>
					<div class="a-cardui-header">
					<h2 class="title mb-3">Offers</h4>
						<div class="a-cardui-body">
							<div>
								<div class="a-section a-spacing-mini center-image aok-block">
									<div class="image-container" style="height:230px">
										<img style="width:200px;height:200px" src="https://m.media-amazon.com/images/I/61uJj7-1HoL._AC_UL600_FMwebp_QL65_.jpg">
									</div>
								</div>
								
											</span>
										</span>
							</div>
						</div>
								
					</div>
						
					</div>
					<h4> 
						<span class="a-size-base-plus _cDEzb_titleTextMaxRows4_2Bk2T"> (2023 New Model) CHEELOM bluetooth headphones, Wireless entertainment headphones</span>  
					</h4>
					<h3>
					<span aria-hidden="true"><span class="a-price-whole"><strong>$238.00</strong></span></span>
					</h3>
					<span style="background-color:#CC0C39;color:#ffffff" class="_deal-of-the-day_style_badgeLabel__USQoX"><span class="a-size-mini">Offer 20%</span></span>
					<div class="a-cardui-footer">
						<a class="a-link-normal see-more truncate-1line" href="#">See more
						</a>
					</div>
				</div>
	</div>
</section>
<section id="paragraphs">
	<div class="container-fluid bg-dark mb-5">
		<div class="container">
			<div><div>
				<div class="row">
					<div class="h-4xl w-full"></div>
					<div class="col-md-10">
						<h3 class="font-bold text-t3 white-text">We are committed to offering you an unparalleled shopping experience. Join thousands of satisfied customers who have discovered the convenience and excitement of shopping with us. Your satisfaction is our priority, so start exploring and find the best deals today!</h3>
					</div>
					<div class="col-md-2">
						<img src="{{ asset('images/nav1.png') }}">
					</div>
					
				</div>
				
				<div class="mt-md sm:mt-3xl"><a class="text-white" href="#" text="Browse all products" url="/mx/productos">Browse all products</a></div></div></div>
				<div class="h-4xl w-full"></div>
		</div>
	</div>
	</div>
</section>
<section>
	<div class="container">
		<h1 class="title text-center">Sales Products</h1>
		<div class="row">
			@php($cat=0)
			@foreach($products as $product)
			@if($product->category_id != $cat)
			@php($cat = $product->category_id)
			<h1>{{ $product->category->name }}</h1>
			@endif
			<div class="col-md-3">
				<div class="card my-2">
					<div class="card" >
						<img class="card-img" style="width: 250px;height:200px;background-color:transparent" src={{ $product->image }}   alt="Card image cap">
						<div class="card-body">
						  <h4 class="card-title"><strong>{{ $product->name }}</strong></h4>
						  <p class="card-text">{{ $product->description }}</p>
						  <p class="card-text"><h1><strong>${{ $product->price }}</strong></h1></p>
						  <a class="a-link-normal" href="#">See more</a>
						</div>
					  </div>
					<div class="card-footer">
						Available: {{ $product->available }}
						
					</div>
				</div>
			</div>
			@endforeach
		</div>
		
	</div>
</section>

<section id="hero">
	<div class="container-fluid hero">
		<div class="container">
			<div class="row d-flex justify-content-between">
				<div class="col-md-3 my-5">
					<img src="{{ asset('/images/index/muscle.jpg') }}" class="img-fluid" alt="">
				</div>
				<div class="col-md-6 my-5 align-self-center">
					<h2 class="title">Might Interest You</h2>
					
					<hr class="hr-white-md">
					<img  style="width: 250px;height:200px;background-color:transparent" src={{ $productlast[0]->image }}   alt="Card image cap">
					<h4 ><strong>{{$productlast[0]->name}}</strong></h4>
					<p >{{ $productlast[0]->description }}</p>
					<p ><h1><strong>${{ $productlast[0]->price }}</strong></h1></p>
				</div>
				<div class="col-md-3 my-5 ms-auto align-self-end right">
					<a href="#" class="btn btn-green">Find Out More >></a>
				</div>
			</div>
		</div>
	</div>
	
</section>
<section id="columns2">
	<div class="container">
		<div class="row mt-5">
			<h1 class="title text-center">Payment Methods</h1>
			<hr class="hr-green-lg">
		</div>
		<div class="row mb-5 text-center">
			<div class="col-md-4">
				<img src="{{ asset('images/index/lowriders.jpg') }}" class="img-fluid" alt="">
				<h3 class="title my-3">Credit and Debit</h3>
				<img alt="Credito y debito" style="width: 200px;height:200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQDlS9CR75KPnuxH5zk05I3sRH4CHCiZ-m1wg&usqp=CAU">
			</div>
			<div class="col-md-4">
				<img src="{{ asset('images/index/tuners.jpg') }}" class="img-fluid" alt="">
				<h3 class="title my-3">Cash</h3>
				<img alt="Pago en efectivo" style="width: 200px;height:200px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABX1BMVEX///8AAACH0Uf50LRw1vmy4m3t9PyJ1EiysrLk5OS+vr6L2El+w0JjmTRAYyKAxkMeLxASHApvqzpTn7n0+/+EiIz/17rAxs38ZX6563FZS0Gn1GZeXl4mSVV03v/1yEycxmB3uD4XIwyp1mcoMxiSuVkxPx5nxuaCpVBzkkdtikNUazSYwV3v7+85OTnQ0NA8TSVhudctRhjowqgSDw38XHcPEwl3d3eqjnvOrJXp6emjo6NedzpZii9qpDh7nEv98tj0xTxGWSsYJg0nPBQmJiYyMjJ9aVsqIx5mZmbVspqPd2e8nYhrWU05Lyn9vMb9rLhLdCf42IdZcDY0URuSkpJPT08dGBVJPTX+3eL+zdX9o7D8hJf8c4r/6+79laVRfiszThr414L879D2zmT54J/76Ln++Or2zFnv+eil3HqX12Gy4I7g8tR8zTDG6azW78QYLjbW8/2o5fs3aHlTUV7zAAAPlElEQVR4nO2d/V8iNx7HBWpHQEToSnfE20UQi4riCSIsuK6o6wOgdden7W577vO1d93etff/vy4PM0MmM5PJSGYG2/n80C4PJnnzTb5JvvkGxsYCBQoUKFCgQIECBQoUKFCgQIH+SnoS9VNPXOdrrYX81dqEu4DHPvNBbbkJeOk3HVLLPcDVmt9wSNfuEVZg+e2iQeVF8Px+WqDmp0GJK4VsNodUxgJVdWAToq4RTsDiC/I4LRk+PxWXxCk+BUrMm9SUg1XF3CXMGqodR9UmpLA4SduwSGNN4/4Qykvw+XmhhGlYZGlkCOEwrMcFAobDcVjVirGb+kOYw8NQLCEciPXCaBDKbfh0WmQnBd30KSx0x2BEXwgLdfBsSiwgECx1wRcb0rOFvAyf3RY5V6D5YhsbkZIHhPUSpR20zkgIFyIMLdEyEp5tbn4vknAUpCN8tfnw4eYPf2bCM0goyoh/dcKUSMEC68an6xThD69fv3kI9Ob1ayEdFREu5Shls9kF8PxGOClO4X1QYme8YJDe0wAns/kQCfzjTBShcZ2B9xbnIhc1eEljXLSNZ3WEPz4kJYrQZG9Rhs8/FbryTlh8mHrCN5sE4I/uEeI5f1Io4TwssmQ0op7wFdlLRQxEK0I4DPMu7C06doRjP52d/QN5mrOznwQAWhGiTnordmEaPzeviyIcc2O2MO4tVoR3UrWbLhuM6AshWgxviO2kABHOFymDrzES/gSG4uYrkYQFermPTJiOi9ZTbERKRsKxV2/eiFqWYsJlWmiZMSVeqNwVSm0joUiN5Lo0IAwIjYS1BaPgeUZ9WqRgTQudzjrW4vqiIi8Iy7R7A4KbmimhnhQitk1qKrlPuGhYZgDBardFTvnxt9CGJnsLDyJRZoTogxW7t7iFRZpU5Q+h/BJWmxQI6Ou5hZkN4TDcF7tskyCJyd7CH8Ki8GGobPKtzki8JpTXhXdS9dzCuLcgCZ+9ePHME0JkwgvRe4twPmR2+EQQPtsFEoxoSohOD3mPntCRBN87UVS/TRuRIPy4+/jx4xfuE8ro3GLDvtmIbXI+PT8Z5sJMophp2ZrwBSDc9YAQbdhsTSjFk+nbDW1huXGbTsZt/gQfPuX976Woj9ocAEtSeopePdenntoYEu3y6RnDe0+D8ltCNnyJaZoPaTrBZMShGmooej1b4AgGu4/G0+Z8UHtplvFxP9VPGV4TYgveMpophbX+WV9vl4rFcrG483JdYzwPMz4dCf9t2y9COYt3a6xBKM2rBuyUsmDzg/9QlrOljtpVmXk42DmtZ2U/CPE0AeZ6RgvjaYWjnaUnNjn7Unlt3voTksLKB6TF9z0kLGMDMi2II4LAfgY+xJhTOutTBqKkTDELxXHZQ0J5vKiOpG1WF1UsSGbFFLLEQgynizE91WAcr5cK3uRiLIIx9DKv1Jpi+ULF29dzBGCuXl8kEcu4HNaRAD5oQyV1dspl9wnrKc0PhqZYjjAsoTG0p1s8l6lIvZxDxe0xp5vJtyG9vIomXjBcRFjd4eXJLZ4MbZYal3WIdbvRDJZ86X0fCKfm2QtLZRASXVQutzvwpLFdJBnlot1QhIVJTy+8JbxIJG13B/uUk8l2tBbml0grooOPfZvSJGlye98bwtp5Yl6y2xeo/mF9AJjT9bMF0t2gecc2VCeBWtPbU+4T7oX5trDoA88NZgk03FLaco30N8g/7vOUKsXn3SfMcwVk8CgcbAtwV+wU4G6yjJbrxK4Ir9+5sqilkSHEjpQwIZxBF3CqfRbHV4uUEbnycUaHkA53olG4JKP/g/kD8hJJ3Hgk+mPDz3cjNISsi4rVMCHKm04RvmbHfsJwi/DdHQnRsUOBItzRbJhbXlraIWYMFOzhiSkLJ3y/qzMiN2Ec7gjIo6Oc0msVQrhF1K1soKflCbkKJ/ywqzMiPyEcaC9JBriYCS3LKiEl5E2nfSD8uPt499NdCJN17FiokRZaX7IgXOYsWTAhCiqTiLyESt6dDmIQmjG7GgY/gDpHWpVQwmcfICBAfKeNRWeERT2FtiytL+cMiCXOOV8g4SeFDyLuvnsvgFAuDczYKcq+E459/qghflSNOEwvhbGPXFsNDugihD4RAsYPyIIfBhPGcITjg6gF7WmVcchbsjhf+gGOQuIx/6qN9qWqAGG+hKN05AeAfOkeR7miCT/vPv4n+Zh/PtwLmd4exHEafNltnSSEbognE0D4jP9p9/3dCGHMweQarxqJQlYkN8HwMc8NRuGEn3UmdLAuRWcq5NRegBczVEK8WyQIuS8Si195f9I94idEDSGCNKgbhqwI+S8Sj87+EKXaE1EavGorygrhCkUIV61792sHjFPtiUP4Anycgt0ReJoy5Wn4E3JGhxC3hIzF4CMK4EVTuSVqtnCQkOMF4V44zhNsky4oI8qDcCnSCmXCKZ5Cpfik+4ShjfPEfNw2YIrjGLr8ybYF4Pgel5+R4tJ8YmrfA0Ko+nnazpBoStSdwcs5zYwLxMobz/92k6EuncObc4vpbebBU1hC/Smk20TIaIXdLpHnbTjrt55kFhZPbhNHXsMS/vzLv/7NQQhGJDtZRMmkKBuSRXQHwsrBDHO2l8LbdV3NQxKCAr/8wiBcHNS0P8lMFrkwItJxGgWQ2UfJdJVUZ3jCX7+AQr/8bE1YkHNLGiXrkDuspBmQHTW7kNedAZfwJ8UyoHSuVtYpFcxuBd2JUDJ/Tb3ZJcs51TGyjoGlJN7wkrtdMoooK+51mjEIpaSSqpBfRvEdAYRjv3yRvvzGJoStyyqMb1mIk9g/LNBBC6WHLmBARgRKmtzD1Swr8SsRhGP/+e1Xi1d0t/Pkcl7pYiwLKENovSjrIWVZTedg/z12MYOUISGE1qLvH64oVrTWIFmkvpTDkW70n9yy6v3PGQ5Zwjmm5KzqMaGy2GR7woTm6VOd9tJOaWep3dEmtxQjWyisJn2RF549JlRD2QlmJkVSc4YGnTMnenwIqb/R7TWhakVmQ8Fy2ZA/q/AxDahkc+ivrHtOqNyXeWuT7RsPJzb0dPW3ibDd8r1OjUF/CJVTJftMivhk+nZqYy9VT01vTN2mJ213J3F0BrlOzTN+EOJMCpvmhlGuPuiU8Fs9pDhHrr6ycqdr84MQ736E3lyDwnEQQ1TZD0KcKiP8Pn4Y1kXfRfCJEBtR9HcqJExN6A8hf56BA6FLpCbfuecPIYqV8ZzCOxGsyuTkwydCtLIRe4cUZYibnM/5Q4irFepN41b3gO0IJ7bm7HTcWmUTmn1PFPSmrHslzgnhEsjs8IpNGD0wXR4aZFkA+h7hUiGrSPsCLpSFcJGcFKckOoDUahoInelULNrH/03OViU8Ud9QqyPh71DL5/N1Vml3lmV7rXoZ/5dxX1kZcUtIy4fVjFXznoMXa3+z039hGVZFoDL81ppl6+Cr3zyy03fwbdZfmO2/FRlfOg9f/uYrOyHC52sDXem/5P1JZcJPVaw9vSNCvWru/+SCGN2ZMHTgd9M5dXfCkMu/KSFKDgiPZwaCy4Rjv9vOJweE3z7QNDYDHs/53XY+OSH8WtMDFuFqNNa6vLxsMT2cdxJM+CQ2c3WtjdQ593Y0/BJIGI3NGFY31zO+G3IIwqtopVKJxWJgym1dbln+XNCxez8e4jahtWqnN7M3g03AnNW25H4Snh42TqqRSCZSbR6OBKNQwqNurxrJZCKKMpHGkfrSlW+MwxHWatfX10dHR6BXHjaa1cyATmHM9GY1Rp8c6xCEN1VFCIWG0xibGuPaxJCONQocW8Wp4xqCcNacyQh5ojHWtu7eWVcvFXd9cOyoEA8IdYxgs3qn3rqqG/nPHTB6QggZ+0QDr2ZalaijHtuindrlqBECxmpX38ba86u5463LVqxiO7RiJssJ7h/L84wQTR43xpZi2oPjliVm9Ep511G3WY2cqIXwWpGP8O8iCKFjrTZmLQOeB6Zb6lU1zHXUw+4aeGc8y3KORU8JMWSz2z81Z7wyxn60eG43QqwkkBmtA4i+EmLKSPWk1+0e9m9O9RY9oNzPhHrkcFjVVVY94jeiH4QKprZKqDYbXWV5p9uRVVQHM3tCr5V68Gm+MIpvhDSuMmUO7BJVf970tGesKIP6+T0iRI1GM6Z6/KDN8NeNiEk9mQZ8jWsBNzqEkSock89xs1rq+Oya8andlGsgjhBhZlbteTHVwfSrVnU07yXhISaszCl8N03LGu6pDREhMcOz3tu/h55G6aWqTB2M9tYT+BbLM9tRJTwh+A4tByAWmiz4tmGjQ0iYsE/P8NQ7q6O+ajNvthaZO7V2MPidPTyVjOjK26rVEXWHXGtYRHzUd54ouyfew72RIAQ7R5sZXntnVTU192+pjwAh2DSquykbB0N8EvzBHp8JQY/s9dVW37AdTETd+oJdhYMgj3+EcLidNAYO9NpkC6F7f1UdqmsuRBOFRTEUoR3woS5qYzMAI5GG6ooc5g94SJg56TUaDbCzn705CulV69rM8JoHDTk+kPSOEC+WzdTv2dgvE1E96Jrzw0gPCWdN4Go33aZd99SmeP0c+PXvD0aNsE/D9bvwuMr27zQPs0V20D9As34fLcJIFc96Rzezh91Gs0qeNLIAVQ9zoPOgv8NmPRoxwoErtTqKM/mTquphqBA3IvyKp596SehYmYh61nFFe5gHsFl/jJoNHQM2VQOaLEKBER9xuZrRJRxMEXNDHR2PLKE2RVwPef4/ooQWU8SfhzCj7pKcHGffI8LBIpT/LPs+EQ6miDkhGXEjR5jpKRsPp7skFwhPeygLSmzETVvDDO1hBBBiX95v9DhXmDx86hQowMOIIkQ6PWwIoMxo23j+QJpXhIiy3+PYCjH4Mloio7AOKpYQUXabdxyXmUxPjSg6izO5SngQrcRax2tUfkwfdlinfBGNT5QHFUOIS1ittKgk7xuuvTvBp0WEhUzxAgmJ4fJkQn9D73q2ccLVYTOZale7vuBKYr8TwgfWhFDR1pWOsnbYi9icQoDuOYhPbblzG84BYevbgSCLyfndaoy/wwLr9Qa57qEtty4tOCCkZZH/GG0d695Wm+1Sax/4oNrsktHFS/duMw5ByPjQK5f62zO1U7D0acJbCpHqSbPX6Ovy2Q5ETvACCW223qDDct30Hyb1201Cvmzy6MQxm7J2HHP9XpQDwolVQrzlr0Yv5yzw1i49ub+HCB9xEd49IhSdmNG72OfHLc/u0MAR/7/v7PQNbNawbYqim24Vh1n6Q+uK7jvW8v0m4d0U4wbkTv8fNbFvEhJD556aEGjC8vanzoL3FxAoaq97zRcoUKBAgQIFChQoUKBAgQIFCnQP9H+PRBGjSw7lyAAAAABJRU5ErkJggg==">
			</div>
			<div class="col-md-4">
				<img src="{{ asset('images/index/sportscl.jpg') }}" class="img-fluid" alt="">
				<h3 class="title my-3">Gift Cards</h3>
				<img alt="Pago en efectivo" style="width: 200px;height:200px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRS02I1WMSWJukIOUVhoinHuJ6d9SnuqWXmeg&usqp=CAU">
			</div>
		</div>
		
	</div>
</section>
<section>
	<div class="container">
	<div class="row featurette">
		<div class="col-md-7">
		  <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
		  <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
		</div>
		<div class="col-md-5">
		  <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="500x500" style="width: 500px; height: 500px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22500%22%20height%3D%22500%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20500%20500%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_189afca1286%20text%20%7B%20fill%3A%23AAAAAA%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A25pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_189afca1286%22%3E%3Crect%20width%3D%22500%22%20height%3D%22500%22%20fill%3D%22%23EEEEEE%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22185.1171875%22%20y%3D%22261.1%22%3E500x500%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
		</div>
	  </div>
	  <hr class="featurette-divider">
	  <div class="row featurette">
		<div class="col-md-7 order-md-2">
		  <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
		  <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
		</div>
		<div class="col-md-5 order-md-1">
		  <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto" alt="500x500" style="width: 500px; height: 500px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22500%22%20height%3D%22500%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20500%20500%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_189afca1288%20text%20%7B%20fill%3A%23AAAAAA%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A25pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_189afca1288%22%3E%3Crect%20width%3D%22500%22%20height%3D%22500%22%20fill%3D%22%23EEEEEE%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22185.1171875%22%20y%3D%22261.1%22%3E500x500%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
		</div>
	  </div>
	</div>
</section>
<section id="paragraphs">
	<div class="container-fluid bg-dark mb-5">
		<div class="container">
			<div class="row">
			<div class="h-4xl w-full"></div>
				<div class="col-md-12 col-sm-12 p-2">
					<h1 class="title-green mt-5">Testimonials</h1>
					<hr class="hr-white">
					<h2 style="color: #ffffff">"LightSpeed ​​Sales' centralized inventory and order management makes it easier for us to deliver orders to customers so we can spend more time growing our business"</h2>
				</div>
				<hr>
				<div class="col-span-6 col-start-2">
					<div class="flex items-start"><figcaption id="caption-1" class="w-full">
						<div class="h-md w-full"></div>
						<div class="mr-12"><img alt="" sizes="100vw" srcset="https://cdn.shopify.com/shopifycloud/brochure/assets/manage/testimonials/prelude-and-dawn-logo-a26a29b5b2f99640eab9498e0af0e5820ae615a3e5d0ceb5a00c1d17e2fffd03.svg" loading="lazy"></div></figcaption></div></div>
				<div class="col-md-6 col-sm-12 right">
					<img src="{{ asset('images/index/engine.jpg') }}" class="img-fluid" alt="">
				</div>
				<div class="h-4xl w-full"></div>
			</div>
		</div>
	</div>
	</div>
</section>
<section class="relative text-center bg-gradient-to-br from-agave-30 to-aloe-30" data-section-name="" data-component-name="">
	<div class="h-4xl w-full"></div>
	<div class="container">
		<div class="flex flex-wrap default:gap-x-gutter gap-y-0 md:items-center">
			<div class="basis-full md:basis-full">
				<h2 class="font-bold text-t2 mb-sm richtext">Start selling today with LightSpeed ​​Sales</h2>
			</div>
			<div class="basis-full md:basis-full">
				<div class="md:mx-24">
					<div class="h-md w-full"></div>
					<p class="text-body-lg mb-xl">Try LightSpeed ​​Sales for free and discover all the tools and services you need to start, run and grow your business.</p>
				</div>
			</div>
			<div class="basis-full md:basis-full">
				<div class="basis-full">
					<div class="flex flex-wrap gap-x-4 justify-center">
						<div class="md:inline-block">
							<a href="#" class="btn btn-green" onclick="document.body.scrollTop = 0; document.documentElement.scrollTop = 5760; event.preventDefault();">Free Trial</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><div class="h-4xl w-full"></div>
	</section>
	
	<hr class="featurette-divider">
	<section>
		<div class="container">
			<div class="row">
				<div style="text-align:center" class="col-lg-4 ">
				  <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
				  <h2>Heading</h2>
				  <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
				  <p><a class="btn btn-green" href="#" role="button">View details »</a></p>
				</div><!-- /.col-lg-4 -->
				<div style="text-align:center" class="col-lg-4">
				  <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
				  <h2>Heading</h2>
				  <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
				  <p><a class="btn btn-green" href="#" role="button">View details »</a></p>
				</div><!-- /.col-lg-4 -->
				<div style="text-align:center" class="col-lg-4">
				  <img class="rounded-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
				  <h2>Heading</h2>
				  <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
				  <p><a class="btn btn-green" href="#" role="button">View details »</a></p>
				</div><!-- /.col-lg-4 -->
			  </div>
		</div>
	</section>
		<!--Section: Contact v.2-->
<section >
	<div class="container-fluid bg-dark mb-5">
		<div class="container">
    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4 white-text">Contact us</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5 white-text" >Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
        a matter of hours to help you.</p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" action="mail.php" method="POST">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="name" class="white-text2">Your name</label>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control">
                            <label for="email" class="white-text2">Your email</label>
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="subject" name="subject" class="form-control">
                            <label for="subject" class="white-text2">Subject</label>
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message" class="white-text2">Your message</label>
                        </div>

                    </div>
                </div>
                <!--Grid row-->

            </form>

           
            <div class="status"></div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p class="white-text2">San Francisco, CA 94126, USA</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p class="white-text2">+ 01 234 567 89</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p class="white-text2">contact@mdbootstrap.com</p>
                </li>
            </ul>
        </div>
		<div class="text-center text-md-left">
			<a class="btn btn-primary btn btn-green">Send</a>
		
		</div>
        <!--Grid column-->
    </div>

    </div>
	</div>
</section>
<!--Section: Contact v.2-->
<section>
	<div class="container">
		<div class="row text-center">
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<div class="card-header">
					  <h4 class="my-0 font-weight-normal">Free</h4>
					</div>
					<div class="card-body">
					  <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
					  <ul class="list-unstyled mt-3 mb-4">
						<li>10 publications included</li>
						<li>2 GB of storage</li>
						<li>Email support</li>
						<li>Help center access</li>
					  </ul>
					  <button type="button" class="btn btn-lg btn-block btn-green-outline-primary">Sign up for free</button>
					</div>
				  </div>
			</div>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<div class="card-header">
					  <h4 class="my-0 font-weight-normal">Pro</h4>
					</div>
					<div class="card-body">
					  <h1 class="card-title pricing-card-title">$15 <small class="text-muted">/ mo</small></h1>
					  <ul class="list-unstyled mt-3 mb-4">
						<li>200 publications included</li>
						<li>10 GB of storage</li>
						<li>Priority email support</li>
						<li>Help center access</li>
					  </ul>
					  <button type="button" class="btn btn-green">Get started</button>
					</div>
				  </div>
			</div>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
					<div class="card-header">
					  <h4 class="my-0 font-weight-normal">Enterprise</h4>
					</div>
					<div class="card-body">
					  <h1 class="card-title pricing-card-title">$29 <small class="text-muted">/ mo</small></h1>
					  <ul class="list-unstyled mt-3 mb-4">
						<li>500 publications included</li>
						<li>15 GB of storage</li>
						<li>Phone and email support</li>
						<li>Help center access</li>
					  </ul>
					  <button type="button" class="btn btn-green">Contact us</button>
					</div>
				  </div>
			</div>
		</div>
		
	  </div>
</section>
@endsection