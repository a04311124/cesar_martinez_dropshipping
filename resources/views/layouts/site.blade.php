<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}" /> 
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <!-- Scripts -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid my-2 px-4 px-lg-5">
                <div class="row">
                    <div class="col-md-4">
                        <a class="navbar-brand" href="{{ route('index') }}">
                            <img class="img-fluid" src="{{ asset('images/nav1.png') }}" width="100px" alt="{{ config('app.name', 'Laravel') }}">
                            <img class="img-fluid" src="{{ asset('images/nav.png') }}" alt="{{ config('app.name', 'Laravel') }}">
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div id="nav-search">
                                <input type="text" style="width: 500px;margin-top:30px;" id="twotabsearchtextbox" value="" name="field-keywords" autocomplete="off" placeholder="Search LightSpeed Deals" class="nav-input nav-progressive-attribute" dir="auto" tabindex="0" aria-label="Buscar LightSpeed" spellcheck="false">
                                <a class="btn btn-green" href="">Search</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    </div>
                    <div class="col-md-2">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">

                            <ul class="navbar-nav mb-2 mb-lg-0" style="width: 300px;margin-top:30px;">
                                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{ route('index') }}">Home</a></li>
                                <li class="nav-item"><a class="nav-link" href="#!">About</a></li>
                                <li class="nav-item"><a class="nav-link" href="#!">Blog</a></li>
                                <li class="nav-item"><a class="nav-link" href="{{ route('dash') }}">Login</a></li>
                                @auth
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Blog</a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a class="dropdown-item" href="#!">All Products</a></li>
                                        <li><hr class="dropdown-divider" /></li>
                                        <li><a class="dropdown-item" href="#!">Popular Items</a></li>
                                        <li><a class="dropdown-item" href="#!">New Arrivals</a></li>
                                    </ul>
                                </li>
                                
                                
                                @endauth
                                
                            </ul>
                        </div>
                    </div>

            </div>
                
        </nav>
        <!-- Content -->
        @yield('content')
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <a href="#" style="text-decoration: none;" id="navBackToTop" aria-label="Inicio de página" onclick="document.body.scrollTop = 0; document.documentElement.scrollTop = 0; event.preventDefault();">
                <div class="m-0 text-center text-white" style="background-color: #202327;">
                <span class="m-0 text-center text-white" style="display: block;text-align:center;padding:15px 0;line-height:19px;">
                  Inicio de página
                </span>
                </div>
              </a>
              <div style="text-align: center">
                <img class="img-fluid" src="{{ asset('images/nav1.png') }}" width="50px" alt="{{ config('app.name', 'Laravel') }}">
                <img class="img-fluid" src="{{ asset('images/nav.png') }}" width="100px" alt="{{ config('app.name', 'Laravel') }}">
                <div class="nav-logo-base nav-sprite"></div>
                </a>
              </div>
              
              <div style="border-top: 1px solid #3a4553"></div>
            <div class="container"><p class="m-0 text-center text-white">Cesar Martinez Lopez <br>Copyright &copy; LightSpeed 2023</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
