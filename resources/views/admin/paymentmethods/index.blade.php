@extends('layouts.panel')


@section('action-button')
<a href="#" class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4" data-bs-toggle="modal" data-bs-target="#CategoryModal"><i class="fa-solid fa-plus"></i> Add Payment Method</a>

@endsection
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-columns me-1"></i>
        Payment Methods
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Imagen</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
            	@forelse($paymentmethods as $paymentmethod)
	                <tr>
	                    <td>{{ $paymentmethod->id }}</td>
                      
	                    <td>{{ $paymentmethod->name }}</td>
                      <td><img class="img-fluid mt-2" style="width: 100px;height:100px;background-color:transparent" src="{{ $paymentmethod->imagen }}" /></td>
	                    <td>@if ($paymentmethod->status == 1)
                            Active
                            @else
                             Deactivate 
                            @endif
                      </td>
	                    <td>
                            <button class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#CategoryModal{{ $paymentmethod->id}}"><i class="fa fa-pencil"></i> Edit</button>
                            @if ($paymentmethod->status == 1)
                            <button class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#CategoryTerminate{{ $paymentmethod->id}}"><i class="fa fa-remove"></i> Deactivate</button>
                            @else
                            <button class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#CategoryActivate{{ $paymentmethod->id}}"><i class="fa fa-check"></i> Activate</button>
                            @endif
							

                        </td>
	                </tr>
                    <div class="modal modal-lg fade" id="CategoryModal{{ $paymentmethod->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modify Payment Method</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('paymentmethods.update')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$paymentmethod->id}}">
                                    <label>Payment Method Name</label>
                                <input class="form-control" type="text" name="name" value="{{ $paymentmethod->name }}">
                                
                                <label>Image</label>
                                <input class="form-control" type="url" name="imagen" placeholder="Put the URL of the Image" value="{{ $paymentmethod->imagen }}">
                                <img class="img-fluid mt-2" style="width: 250px;height:200px;background-color:transparent" src="{{ $paymentmethod->imagen }}" />
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="CategoryTerminate{{ $paymentmethod->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Deactivate Payment Method</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('paymentmethods.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$paymentmethod->id}}">
                                <input type="hidden" name="status" value="0">
                                <h2>¿Are you sure. you want to Deactivate this Payment Method?</h2>

                                <h3>{{ $paymentmethod->name }} </h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="CategoryActivate{{ $paymentmethod->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Activate Payment Method</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('paymentmethods.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$paymentmethod->id}}">
                                <input type="hidden" name="status" value="1">
                                <h2>¿Are you sure. you want to Activate this Payment Method?</h2>

                                <h3>{{ $paymentmethod->name }} </h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>
                @endforeach
                
            </tbody>
        </table>
        {{$paymentmethods->links()}}
    </div>
</div>

<div class="modal modal-lg fade" id="CategoryModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Payment Method</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form method="POST" action="{{route('paymentmethods.create')}}">
                  @csrf
                  <label>Payment Method Name</label>
              <input class="form-control" type="text" name="name" >

              <label>Image</label>
                                <input class="form-control" type="url" name="imagen" placeholder="Put the URL of the Image" >
                                <img class="img-fluid mt-2" style="width: 250px;height:200px;background-color:transparent"  />
                                
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
        </div>
      </div>
  </div>

@endsection

