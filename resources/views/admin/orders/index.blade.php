@extends('layouts.panel')

@section('action-button')
<br>
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ProductModal">
	Export
	  </button>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-file me-1"></i>
        Report Orders
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Date Order</th>
					<th>Client</th>
                    <th>Product</th>
                    <th>Price<th>
                </tr>
            </thead>
            <tbody>
            	@forelse($orders as $order)
	                <tr>
	                    <td>{{ $order->id }}</td>
	                    <td>{{ $order->date_order }}</td>
						<td>{{ $order->client->name }}</td>
                        <td>{{ $order->product->name }}</td>
                        <td>${{ $order->product->price }}</td>
	                </tr>

                      
                @endforeach
                
            </tbody>
        </table>
        {{$orders->links()}}
    </div>
</div>
<div class="col">


		<!-- dropdown-menu -->
		
		

</div>

@endsection
