@extends('layouts.panel')


@section('action-button')
<a href="#" class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4" data-bs-toggle="modal" data-bs-target="#CategoryModal"><i class="fa-solid fa-plus"></i> Add Category</a>

@endsection
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-columns me-1"></i>
        Categories
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
            	@forelse($categories as $category)
	                <tr>
	                    <td>{{ $category->id }}</td>
	                    <td>{{ $category->name }}</td>
	                    <td>@if ($category->status == 1)
                            Active
                            @else
                             Deactivate 
                            @endif
                      </td>
	                    <td>
                            <button class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#CategoryModal{{ $category->id}}"><i class="fa fa-pencil"></i> Edit</button>
                            @if ($category->status == 1)
                            <button class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#CategoryTerminate{{ $category->id}}"><i class="fa fa-remove"></i> Deactivate</button>
                            @else
                            <button class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#CategoryActivate{{ $category->id}}"><i class="fa fa-check"></i> Activate</button>
                            @endif
							

                        </td>
	                </tr>
                    <div class="modal modal-lg fade" id="CategoryModal{{ $category->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modify Category</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('categories.update')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$category->id}}">
                                    <label>Category Name</label>
                                <input class="form-control" type="text" name="name" value="{{ $category->name }}">
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="CategoryTerminate{{ $category->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Deactivate Category</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('categories.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$category->id}}">
                                <input type="hidden" name="status" value="0">
                                <h2>¿Are you sure. you want to Deactivate this Category?</h2>

                                <h3>{{ $category->name }} </h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="CategoryActivate{{ $category->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Activate Category</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('categories.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$category->id}}">
                                <input type="hidden" name="status" value="1">
                                <h2>¿Are you sure. you want to Activate this Category?</h2>

                                <h3>{{ $category->name }} </h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>
                @endforeach
                
            </tbody>
        </table>
        {{$categories->links()}}
    </div>
</div>

<div class="modal modal-lg fade" id="CategoryModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Category</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form method="POST" action="{{route('categories.create')}}">
                  @csrf
                  <label>Category Name</label>
              <input class="form-control" type="text" name="name" >
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
        </div>
      </div>
  </div>

@endsection

