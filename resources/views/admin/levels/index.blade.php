@extends('layouts.panel')


@section('action-button')
<a href="#" class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4" data-bs-toggle="modal" data-bs-target="#LevelModal"><i class="fa-solid fa-plus"></i> Add Level</a>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-level-up me-1"></i>
        Levels
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
            	@forelse($levels as $level)
	                <tr>
	                    <td>{{ $level->id }}</td>
	                    <td>{{ $level->name }}</td>
	                    <td>{{ $level->code }}</td>
	                    <td>{{ $level->description }}</td>
	                    <td>
                            <button class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#LevelModal{{ $level->id}}"><i class="fa fa-pencil"></i> Edit</button>
                        </td>
	                </tr>
                    <div class="modal modal-lg fade" id="LevelModal{{ $level->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modify Level</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('levels.update')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$level->id}}">
                                    <label>Level Name</label>
                                <input class="form-control" type="text" name="name" value="{{ $level->name }}">
                    
                    
                                <label>Level Code</label>
                                <input class="form-control" type="text" name="code" value="{{ $level->code }}">

                                <label>Level Description</label>
                                <input class="form-control" type="text" name="description" value="{{ $level->description }}">
                    
                    
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>
                @endforeach
                
            </tbody>
        </table>
        {{$levels->links()}}
    </div>
</div>

<div class="modal modal-lg fade" id="LevelModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Level</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form method="POST" action="{{route('levels.create')}}">
                  @csrf
                  <label>Level Name</label>
              <input class="form-control" type="text" name="name" >
  
  
              <label>Level Code</label>
              <input class="form-control" type="text" name="code" >

              <label>Level Description</label>
              <input class="form-control" type="text" name="description">
  
  
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
        </div>
      </div>
  </div>

@endsection