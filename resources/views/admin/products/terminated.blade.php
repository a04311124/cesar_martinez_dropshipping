@extends('layouts.panel')

@section('title', 'Dashboard')


@section('name', 'Products')

@section('action-button')
<br>
@if(auth()->user()->level_id  == 4)
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#ProductModal">
   + Add Product
  </button>
@endsection
@endif
@section('content')
<br>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-boxes me-1"></i>
        Productos
    </div>
    <div class="card-body">
		@if(auth()->user()->level_id  == 4)
<ul class="nav nav-tabs mb-3">
	<li class="nav-item">
		<a class="nav-link" href="{{ route('products') }}">Active</a>
	</li>
	<li class="nav-item">
		<a class="nav-link active" aria-current="page" href="{{ route('products.terminated') }}">Terminated</a>
	</li>
</ul>
@endif

@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
	@foreach($products as $product)
			<div class="col-md-3">
				<div class="card my-2">
					<div class="card" >
						<img class="card-img" style="width: 250px;height:200px;background-color:transparent" src={{ $product->image }}   alt="Card image cap">
						<div class="card-body">
						  <h4 class="card-title"><strong>{{ $product->name }}</strong></h4>
						  <p class="card-text">{{ $product->description }}</p>
						  <p class="card-text"><h1><strong>${{ $product->price }}</strong></h1></p>
						  @if(auth()->user()->level_id  == 4)
						  <button class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ProductModal{{ $product->id}}"><i class="fa fa-pencil"></i> Edit</button>
							<button class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#ProductModalTerminate{{ $product->id}}"><i class="fa fa-remove"></i> Activate</button>
							@endif
						</div>
					  </div>
					<div class="card-footer">
						Available: {{ $product->available }}
						
					</div>
				</div>
			
			</div>

<!-- Modal Edit-->
		<div class="modal modal-lg fade" id="ProductModalTerminate{{$product->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Activate Product</h5>
		  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<div class="modal-body">
			<form method="POST" action="{{route('products.terminate')}}">
				@csrf
			<input type="hidden" name="id" value="{{$product->id}}">
			<input type="hidden" name="status" value="1">
			
			<h2>¿Are you sure. you want to Activate this Product?</h2>

			<h3>{{ $product->name }} {{ $product->description }} </h3>

			
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		  <button type="submit" class="btn btn-primary">Save changes</button>
		</form>
		</div>
	
	  </div>
	</div>
  </div>

  <div class="modal modal-lg fade" id="ProductModal{{ $product->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">Modify Product</h5>
		  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<div class="modal-body">
			<form method="POST" action="{{route('products.update')}}">
				@csrf
			<input type="hidden" name="id" value="{{$product->id}}">
				<label>Product Name</label>
			<input class="form-control" type="text" name="name" value="{{ $product->name }}">

			<label>Category</label>
	      	<select class="form-control"  name="category_id">
				<option value="{{ $product->category->id }}">{{ $product->category->name }}</option>
	      		@foreach($categories as $category)
					@if($category->id != $product->category_id)
	      				<option value="{{ $category->id }}">{{ $category->name}}</option>
					@endif
	      		@endforeach
	      	</select>


			<label>Product Description</label>
			<input class="form-control" type="text" name="description" value="{{ $product->description }}">

			<label>Product Price</label>
			<input class="form-control" type="text" name="price" value="{{ $product->price }}">

			<label>Product Available</label>
			<input class="form-control" type="text" name="available" value="{{ $product->available }}">

			<label>Image</label>
			<input class="form-control" type="url" name="image" placeholder="Put the URL of the Image" value="{{ $product->image }}">
			<img class="img-fluid mt-2" style="width: 250px;height:200px;background-color:transparent" src="{{ $product->image }}" alt="Imagen del Producto" />
			
			
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		  <button type="submit" class="btn btn-primary">Save changes</button>
		</div>
	</form>
	  </div>
	</div>
  </div>



@endforeach
</div>

<!-- Modal -->
<div class="modal modal-lg fade" id="ProductModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		
		<div class="modal-header">
		  <h5 class="modal-title" id="exampleModalLabel">New Product</h5>
		  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<div class="modal-body">
			
			<form method="POST" action="{{route('products.create')}}">
			@csrf
				<label>Product Name</label>
			<input class="form-control" type="text" name="name">

			<label>Category</label>
	      	<select class="form-control"  name="category_id">
	      		<option>Select category</option>
	      		@foreach($categories as $category)
	      		<option value="{{ $category->id }}">{{ $category->name}}</option>
	      		@endforeach
	      	</select>


			<label>Product Description</label>
			<input class="form-control" type="text" name="description">

			<label>Product Price</label>
			<input class="form-control" type="text" name="price">

			<label>Product Available</label>
			<input class="form-control" type="text" name="available">

			<label>Image</label>
			<input class="form-control" type="url" name="image" placeholder="Put the URL of the Image">
			<img class="img-fluid mt-2" src="https://th.bing.com/th/id/OIP.5nFZ_DW_jC2iI94WqT7VFwHaHa?pid=ImgDet&rs=1" alt="Imagen del Producto" />
			
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		  <button type="submit" class="btn btn-primary">Save changes</button>
		</div>
	</form>
	  </div>
	</div>
  </div>

  
	</div>
</div>
  @endsection