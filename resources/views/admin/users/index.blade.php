@extends('layouts.panel')


@section('breadcrumbs', '')

@section('action-button')
<button class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4" type="button" data-bs-toggle="modal" data-bs-target="#addVehicleModal"><i class="fa-solid fa-plus"></i> Add User</button>
@endsection

@section('content')
<br>
@if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
@endif

@if (\Session::has('error'))
    <div class="alert alert-danger">
        {!! \Session::get('error') !!}
    </div>
@endif

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-users me-1"></i>
        Users
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Level</th>
                </tr>
            </thead>
            <tbody>
            	@forelse($users as $user)
	                <tr>
	                    <td></td>
	                    <td>{{ $user->name }}</td>
	                    <td>{{ $user->phone }}</td>
	                    <td>{{ $user->email }}</td>
	                    <td>{{ $user->level->name }}</td>
	                </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>



<!-- Add New Modal -->
<div class="modal fade" id="addVehicleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="modal-title fs-5" id="exampleModalLabel">Add New User</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
			
				<form action="{{ route('users.create') }}" method="POST">
					@csrf
					<label>Name</label>
					<input name="name" class="form-control" type="text" placeholder="Enter Name" autocomplete="off" required>

					<label>Phone</label>
					<input name="phone" class="form-control" type="text" placeholder="Enter Phone Number" autocomplete="off" required>

					<label>Email</label>
					<input name="email" class="form-control" type="email" placeholder="Enter Email" autocomplete="off" required>

					<label>Access Level</label>
					<select name="level_id" class="form-control" required>
						<option value="0">Select Level Access</option>
						@foreach($levels as $level)
							<option value="{{ $level->id }}">{{ $level->name }}</option>
						@endforeach
					</select>
					
					<p class="mt-2">User Password is default to: </p>
					<input name="password" class="form-control" type="password" placeholder="Enter Password" autocomplete="off" required>
					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
