@extends('layouts.panel')


@section('action-button')
<a href="#" class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4" data-bs-toggle="modal" data-bs-target="#CategoryModal"><i class="fa-solid fa-plus"></i> Add Frequent Questions</a>

@endsection
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-question me-1"></i>
        Frequent Questions
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Ask</th>
                    <th>Answer</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
            	@forelse($frecuentquestions as $frecuentquestion)
	                <tr>
	                    <td>{{ $frecuentquestion->id }}</td>
                      
	                    <td>{{ $frecuentquestion->ask }}</td>
                      <td>{{ $frecuentquestion->answer }}</td>
	                    <td>@if ($frecuentquestion->status == 1)
                            Active
                            @else
                             Deactivate 
                            @endif
                      </td>
	                    <td>
                            <button class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#CategoryModal{{ $frecuentquestion->id}}"><i class="fa fa-pencil"></i> Edit</button>
                            @if ($frecuentquestion->status == 1)
                            <button class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#CategoryTerminate{{ $frecuentquestion->id}}"><i class="fa fa-remove"></i> Deactivate</button>
                            @else
                            <button class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#CategoryActivate{{ $frecuentquestion->id}}"><i class="fa fa-check"></i> Activate</button>
                            @endif
							

                        </td>
	                </tr>
                    <div class="modal modal-lg fade" id="CategoryModal{{ $frecuentquestion->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modify Frequent Question</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('frecuentquestions.update')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$frecuentquestion->id}}">
                                    <label>Ask</label>
                                <input class="form-control" type="text" name="ask" value="{{ $frecuentquestion->ask }}">

                                <label>Answer</label>
                            <input class="form-control" type="text" name="answer" value="{{ $frecuentquestion->answer }}">
                                
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="CategoryTerminate{{ $frecuentquestion->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Deactivate Frequent Question</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('frecuentquestions.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$frecuentquestion->id}}">
                                <input type="hidden" name="status" value="0">
                                <h2>¿Are you sure. you want to Deactivate this Question?</h2>

                                <h3>{{ $frecuentquestion->ask }} <br> {{ $frecuentquestion->answer }}</h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="CategoryActivate{{ $frecuentquestion->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Activate Frequent Question</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('frecuentquestions.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$frecuentquestion->id}}">
                                <input type="hidden" name="status" value="1">
                                <h2>¿Are you sure. you want to Activate this Question?</h2>

                                <h3>{{ $frecuentquestion->ask }} <br> {{ $frecuentquestion->answer }}</h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>
                @endforeach
                
            </tbody>
        </table>
        {{$frecuentquestions->links()}}
    </div>
</div>

<div class="modal modal-lg fade" id="CategoryModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Frequent Question</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form method="POST" action="{{route('frecuentquestions.create')}}">
                  @csrf
                  <label>Ask</label>
              <input class="form-control" type="text" name="ask" >

              <label>Answer</label>
              <input class="form-control" type="text" name="answer" >

              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
        </div>
      </div>
  </div>

@endsection

