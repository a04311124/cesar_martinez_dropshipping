@extends('layouts.panel')

@section('page', 'Site Management')

@section('action-button')
<a href="#" class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4"><i class="fa-solid fa-plus"></i> Add Section</a>
@endsection

@section('content')


@foreach($sections as $section)


	<div class="row">
		<div class="col-md-6 col-sm-12" id="prg2">
			<img src="{{ $section->image }}" class="img-fluid" alt="">
		</div>
		<div class="col-md-6 col-sm-12 right">
			<h1 class="title-dark mt-5">
				{{ $section->title }}
			</h1>
			<hr class="hr-orange">
			<p class="dark-text">{{ $section->paragraph}}</p>
		</div>
	</div>

	<!-- Button trigger modal -->
<button type="button" class="btn btn-warning mt-4" data-bs-toggle="modal" data-bs-target="#sectionModal-{{$section->id}}">
  Editar
</button>

<!-- Modal -->
<div class="modal fade" id="sectionModal-{{$section->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="sectionModalLabel">Edit Section</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('sections.update') }}">
        	@csrf
        	<input type="text" class="form-control mb-1" name="title" value="{{ $section->title }}">

        	<input type="text" class="form-control mb-1" name="image" value="{{ $section->image }}">

        	<textarea class="form-control mb-1" name="paragraph">{{ $section->paragraph}}</textarea>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>


@endforeach




@endsection