@extends('layouts.panel')

@section('page', 'Dashboard')

@section('breadcrumb')

@section('content')

<div class="row">
    <div class="col-xl-3 col-md-4">
        <div class="card bg-primary text-white mb-4">
            <div class="card-body">
                <h5>Products</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-boxes fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $products }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('products') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    @if(auth()->user()->level_id  == 4)
    <div class="col-xl-3 col-md-4">
        <div class="card bg-success text-white mb-4">
            <div class="card-body">
                <h5>Clients</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-user-tag fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $clients }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('clients') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    
    <div class="col-xl-3 col-md-4">
        <div class="card bg-warning text-white mb-4">
            <div class="card-body">
                <h5>Categories</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-columns fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $categories }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('categories') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-4">
        <div class="card bg-info text-white mb-4">
            <div class="card-body">
                <h5>Payment Methods</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-credit-card fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $paymentmethods }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('paymentmethods') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-4">
        <div class="card bg-black text-white mb-4">
            <div class="card-body">
                <h5>Levels</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-level-up fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $levels }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('levels') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
   
    <div class="col-xl-3 col-md-4">
        <div class="card bg-primary text-white mb-4">
            <div class="card-body">
                <h5>Users</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-users fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $users }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('users') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    @endif
    <div class="col-xl-3 col-md-4">
        <div class="card bg-success text-white mb-4">
            <div class="card-body">
                <h5>Frequent Questions</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-question fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $frecuentquestions }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('frecuentquestions') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-4">
        <div class="card bg-warning text-white mb-4">
            <div class="card-body">
                <h5>Reports</h5>
                <div class="row mt-3">
                    <div class="col">
                        <i class="fas fa-file fa-2x"></i>
                    </div>
                    <div class="col" style="text-align:right;">
                        <h3>{{ $orders }}</h3>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex align-items-center justify-content-between">
                <a class="small text-white stretched-link" href="{{ route('order') }}">Manage</a>
                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    
    
</div>

@endsection