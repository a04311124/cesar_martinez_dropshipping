@extends('layouts.panel')


@section('action-button')
<a href="#" class="d-none d-sm-inline-block btn btn-primary shadow-sm mt-4" data-bs-toggle="modal" data-bs-target="#ClientModal"><i class="fa-solid fa-plus"></i> Add Client</a>

@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
    	<p class="bold">Errors found:</p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-columns me-1"></i>
        Clients
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-sm" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
					<th>Address</th>
					<th>Email</th>
					<th>Phone</th>

                    <th>Status</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
            	@forelse($clients as $client)
	                <tr>
	                    <td>{{ $client->id }}</td>
	                    <td>{{ $client->name }}</td>
						<td>{{ $client->address }}</td>
						<td>{{ $client->email }}</td>
						<td>{{ $client->phone }}</td>
	                    <td>@if ($client->status == 1)
                            Active
                            @else
                             Deactivate 
                            @endif
                      </td>
	                    <td>
                            <button class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#ClientModal{{ $client->id}}"><i class="fa fa-pencil"></i> Edit</button>
                            @if ($client->status == 1)
                            <button class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#ClientTerminate{{ $client->id}}"><i class="fa fa-remove"></i> Deactivate</button>
                            @else
                            <button class="btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#ClientActivate{{ $client->id}}"><i class="fa fa-check"></i> Activate</button>
                            @endif
							

                        </td>
	                </tr>
                    <div class="modal modal-lg fade" id="ClientModal{{ $client->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modify Client</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('clients.update')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$client->id}}">
                                    <label>Client Name</label>
                                <input class="form-control" type="text" name="name" value="{{ $client->name }}">
							
                                    <label>Client Address</label>
                                <input class="form-control" type="text" name="address" value="{{ $client->address }}">
								
                                    <label>Client Email</label>
                                <input class="form-control" type="text" name="email" value="{{ $client->email }}">
							
                                    <label>Client Phone</label>
                                <input class="form-control" type="text" name="phone" value="{{ $client->phone }}">
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="ClientTerminate{{ $client->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Deactivate Client</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('clients.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$client->id}}">
                                <input type="hidden" name="status" value="0">
                                <h2>¿Are you sure. you want to Deactivate this Client?</h2>

                                <h3>{{ $client->name }} </h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>

                      <div class="modal modal-lg fade" id="ClientActivate{{ $client->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Activate Client</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{route('clients.terminate')}}">
                                    @csrf
                                <input type="hidden" name="id" value="{{$client->id}}">
                                <input type="hidden" name="status" value="1">
                                <h2>¿Are you sure. you want to Activate this Client?</h2>

                                <h3>{{ $client->name }} </h3>
                                
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                          </div>
                        </div>
                      </div>
                @endforeach
                
            </tbody>
        </table>
        {{$clients->links()}}
    </div>
</div>

<div class="modal modal-lg fade" id="ClientModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Client</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <form method="POST" action="{{route('clients.create')}}">
                  @csrf
                  <label>Client Name</label>
              <input class="form-control" type="text" name="name" >

			  <label>Client Address</label>
			  <input class="form-control" type="text" name="address">
			  
				  <label>Client Email</label>
			  <input class="form-control" type="text" name="email">
		  
				  <label>Client Phone</label>
			  <input class="form-control" type="text" name="phone" >
			  
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
      </form>
        </div>
      </div>
  </div>

@endsection